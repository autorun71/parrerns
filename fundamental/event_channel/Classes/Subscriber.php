<?php

include_once __DIR__ . '/../Interfaces/SubscriberInterface.php';
class Subscriber implements SubscriberInterface
{

    /**
     * @var string
     */
    private $name;

    /**
     * Subscriber constructor.
     * @param $name
     */
    function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    function getName()
    {
        return $this->name;
    }

    /**
     * @param $data
     * @return mixed|void
     */
    function notify($data)
    {
        $msg = "{$this->getName()} оповещен данными [{$data}]";

        echo "<pre>";
        print_r($msg);
        echo "</pre>";
    }
}