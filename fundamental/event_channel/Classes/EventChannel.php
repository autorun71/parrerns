<?php

include_once __DIR__ . '/../Interfaces/EventChannelInterface.php';

class EventChannel implements EventChannelInterface
{

    private $topics = [];

    /**
     * @inheritDoc
     */
    function publish($topic, $data)
    {
        if (empty($this->topics[$topic])){
            return;
        }

        foreach ($this->topics[$topic] as $subscriber) {
            /**  @var SubscriberInterface $subscriber */
            $subscriber->notify($data);
            
        }
    }

    /**
     * @inheritDoc
     */
    function subscribe($topic, SubscriberInterface $subscriber)
    {
        $this->topics[$topic][] = $subscriber;

        $msg = "{$subscriber->getName()} подписан на [{$topic}]";
        echo "<pre>";
        print_r($msg);
        echo "</pre>";
    }
}