<?php

include_once __DIR__ . '/../Interfaces/PublisherInterface.php';
include_once __DIR__ . '/../Interfaces/EventChannelInterface.php';

class Publisher implements  PublisherInterface
{
    /**
     * @var string
     */
    private $topic;

    /**
     * @var EventChannelInterface
     */
    private $eventChannel;

    /**
     * Publisher constructor.
     * @param $topic
     * @param EventChannelInterface $eventChannel
     */
    public function __construct($topic, EventChannelInterface $eventChannel)
    {
        $this->topic = $topic;
        $this->eventChannel = $eventChannel;
    }

    /**
     * @param string $data
     * @return mixed|void
     */
    function publish($data)
    {
        $this->eventChannel->publish($this->topic, $data);
    }
}