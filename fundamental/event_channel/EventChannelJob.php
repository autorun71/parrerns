<?php

include_once 'Classes/EventChannel.php';
include_once 'Classes/Publisher.php';
include_once 'Classes/Subscriber.php';

class EventChannelJob
{

    function run()
    {
        $newsChannel = new EventChannel();

        $highGardenGroup = new Publisher('highgarden-news', $newsChannel);

        $winterFellNews = new Publisher('winterfell-news', $newsChannel);
        $winterFellDaily = new Publisher('winterfell-news', $newsChannel);

        $sansa = new Subscriber('Sansa Stark');
        $arya = new Subscriber('Arya Stark');
        $cersei = new Subscriber('Cersei Lannister');
        $show = new Subscriber('John Show');

        $newsChannel->subscribe('highgarden-news', $cersei);
        $newsChannel->subscribe('winterfell-news', $sansa);

        $newsChannel->subscribe('highgarden-news', $arya);
        $newsChannel->subscribe('winterfell-news', $arya);

        $newsChannel->subscribe('winterfell-news', $show);

        $highGardenGroup->publish('New highgarden post');
        $winterFellNews->publish('New winterfell post');
        $winterFellDaily->publish('New alternative winterfell post');

    }

}