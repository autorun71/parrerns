<?php


interface PublisherInterface
{
    /**
     * Уведомление подписчиков
     *
     * @param string $data Данные, которыми уведомляются подписчики
     * @return mixed
     */
    function publish($data);
}