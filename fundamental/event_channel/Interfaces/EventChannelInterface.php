<?php

include_once 'SubscriberInterface.php';

interface EventChannelInterface
{

    /**
     * Издатель уведомляет канал о том, что надо
     * всех, кто подписан на тему $topic уведомить данными $data
     *
     * @param string $topic
     * @param $data
     * @return mixed
     */
    function publish($topic, $data);

    /**
     * Подписчик $subscriber подписывается на событие (данные, инфу и тп) $topic
     *
     * @param string $topic
     * @param SubscriberInterface $subscriber
     * @return mixed
     */
    function subscribe($topic, SubscriberInterface $subscriber);
}