<?php


interface SubscriberInterface
{

    /**
     * Получение имени подписчика
     *
     * @return string
     */
    function getName();

    /**
     * Уведомление подписчика
     *
     * @param $data
     * @return mixed|void
     */
    function notify($data);
}