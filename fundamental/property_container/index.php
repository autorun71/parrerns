<?php
include 'Test.php';

$obj = new Test();

$obj->addProperty('test1', 'test_value1');
$obj->addProperty('test2', 'test_value2');
$obj->setProperty('test1', 'test_value3');

$obj->deleteProperty('test2');

echo "<pre>";
print_r($obj);
echo "</pre>";