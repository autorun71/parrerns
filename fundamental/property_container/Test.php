<?php

interface PropertyContainerInterface
{
    function addProperty($key, $value);

    function getProperty($key);

    function deleteProperty($key);

    function setProperty($key, $value);
}

class PropertyContainer implements PropertyContainerInterface
{
    private $propertyContainer = [];

    function addProperty($key, $value)
    {
        $this->propertyContainer[$key] = $value;
    }

    function getProperty($key)
    {
        return $this->propertyContainer[$key] ?? null;
    }

    function deleteProperty($key)
    {
        unset($this->propertyContainer[$key]);
    }

    function setProperty($key, $value)
    {
        if (!isset($this->propertyContainer[$key])) {
            throw new \Exception("Property [{$key}] not found!");
        }
        $this->propertyContainer[$key] = $value;
    }
}

class Test extends PropertyContainer
{

}