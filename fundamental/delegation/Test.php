<?php

interface MessengerInterface
{
    function setSender($value): MessengerInterface;

    function setRecipient($value): MessengerInterface;

    function setMessage($value): MessengerInterface;

    function send(): bool;
}

abstract class AbstractMessenger implements MessengerInterface
{
    protected $sender;
    protected $recipient;
    protected $message;

    function setSender($value): MessengerInterface
    {
        $this->sender = $value;
        return $this;
    }

    function setRecipient($value): MessengerInterface
    {
        $this->recipient = $value;
        return $this;
    }

    function setMessage($value): MessengerInterface
    {
        $this->message = $value;

        return $this;
    }

    function send(): bool
    {
        return true;
    }
}

class EmailMessenger extends AbstractMessenger
{
    function send(): bool
    {
        echo "<pre>";
        print_r('Sent by ' .__CLASS__ . '::' . __METHOD__);
        echo "</pre>";
        return parent::send();
    }
}

class SMSMessenger extends AbstractMessenger
{
    function send(): bool
    {
        echo "<pre>";
        print_r('Sent by ' .__CLASS__ . '::' . __METHOD__);
        echo "</pre>";
        return parent::send();
    }
}

class AppMessenger implements MessengerInterface
{
    /**
     * @var MessengerInterface
     */
    private $messenger;

    function __construct()
    {
        $this->toSms();
    }

    private function toEmail()
    {
        $this->messenger = new EmailMessenger();

        return $this;
    }

    private function toSms()
    {
        $this->messenger = new SMSMessenger();

        return $this;
    }

    function setSender($value): MessengerInterface
    {
        return $this->messenger->setSender($value);
    }

    function setRecipient($value): MessengerInterface
    {
        return $this->messenger->setRecipient($value);
    }

    function setMessage($value): MessengerInterface
    {
        return $this->messenger->setMessage($value);
    }

    function send(): bool
    {
        return $this->messenger->send();
    }
}